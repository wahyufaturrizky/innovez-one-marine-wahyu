/* eslint-disable */

import React, {useEffect, useState} from 'react';
import {Appbar} from 'react-native-paper';
import {Image, View, StyleSheet, Dimensions} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {color} from 'react-native-reanimated';

function appbarHome(props) {
  const [haveProduk, setHaveProduk] = useState(false);

  const logoHorizontal = '../assets/images/dplaza2.png';
  let title = '';
  let likeProduk = '';

  if (props.params.route != null) {
    title = props.params.route.params.title;
  }

  if (props.like != null) {
    likeProduk = props.like;
    console.log(likeProduk);
  }

  const gotoPesanan = () => {
    props.params.navigation.navigate('PesananSaya', {title: 'Pesanan Saya'});
  };

  const gotoWishlist = () => {
    props.params.navigation.navigate('Wishlist', {title: 'Produk Saya'});
  };

  const gotoNotifikasi = () => {
    props.params.navigation.navigate('Notifikasi', {title: 'Notifikasi'});
  };

  const {height, width} = Dimensions.get('window');

  return (
    <Appbar.Header
      style={[
        styles.shadow,
        {
          backgroundColor:
            title === 'Bagikan Momen Kebaikanmu'
              ? '#16212D'
              : title === 'Login OTP'
              ? '#F84A8F'
              : title === 'Input Nominal Donate'
              ? '#16212D'
              : 'white',
          width: '100%',
          height: 60,
          position: 'relative',
          top: 0,
        },
      ]}>
      {title !== 'Home' &&
        title !== 'Login OTP' &&
        title !== 'Input Nominal Donate' &&
        title !== 'Jualan Anda' &&
        title !== 'Settings' &&
        title !== 'Pesanan Saya' && (
          <Appbar.BackAction
            style={[
              {
                color: title === 'Bagikan Momen Kebaikanmu' ? 'white' : 'black',
              },
            ]}
            onPress={() => {
              props.params.navigation.goBack();
            }}
          />
        )}

      {title === 'Input Nominal Donate' || title === 'Login OTP' ? (
        <Appbar.Action
          icon={title === 'Login OTP' ? 'chevron-left' : 'close'}
          style={[
            styles.shadow,
            {
              color: 'white',
              backgroundColor: title === 'Login OTP' ? 'white' : null,
              borderRadius: 10,
            },
          ]}
          onPress={() => {
            props.params.navigation.goBack();
          }}
        />
      ) : (
        <Appbar.Content />
      )}

      {(title === 'Home' ||
        title === 'Jualan Anda' ||
        title === 'Edit Profil' ||
        title === 'Settings') && (
        <Image
          source={require(logoHorizontal)}
          style={{
            width: 170,
            height: 45,
            resizeMode: 'contain',
            marginBottom: 0,
          }}
          width={180}
          height={25}
        />
      )}

      {title !== 'Detail Campaign' &&
      title !== 'Jualan Anda' &&
      title !== 'Edit Profil' &&
      title !== 'Settings' &&
      title !== 'Login OTP' &&
      title !== 'Input Nominal Donate' &&
      title !== 'Choose Your Plan' &&
      title !== 'Opsi Pembayaran' &&
      title !== 'Bagikan Momen Kebaikanmu' ? (
        <Appbar.Content titleStyle={{fontSize: 14}} title={title} />
      ) : (
        <Appbar.Content />
      )}

      {title === 'Home' && (
        <TouchableOpacity onPress={gotoNotifikasi}>
          <Appbar.Action size={30} icon="bell-ring-outline" />
        </TouchableOpacity>
      )}

      {title !== 'Home' &&
        (title === 'Jualan Anda' && haveProduk ? (
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity onPress={gotoPesanan}>
              <Appbar.Action
                size={30}
                icon="cart"
                color={haveProduk ? 'white' : 'black'}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={gotoNotifikasi}>
              <Appbar.Action
                size={30}
                icon="bell-ring-outline"
                color={haveProduk ? 'white' : 'black'}
              />
            </TouchableOpacity>
          </View>
        ) : (
          title !== 'Bantuan Jualan' &&
          title !== 'Bantuan Foto' &&
          title !== 'Bantuan Judul' &&
          title !== 'Bantuan Lain' && (
            <View style={{flexDirection: 'row'}}>
              {/* ---- [Deactive Icon Button Love] ---- */}
              {/* <TouchableOpacity onPress={gotoWishlist}>
                <Appbar.Action
                  size={30}
                  icon="heart"
                  color={likeProduk || title === 'Produk Saya' ? 'red' : 'gray'}
                />
              </TouchableOpacity> */}
            </View>
          )
        ))}
    </Appbar.Header>
  );
}

export default appbarHome;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
