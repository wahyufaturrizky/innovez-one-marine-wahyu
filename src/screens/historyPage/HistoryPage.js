/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import {
  Text,
  Card,
  Chip,
  Avatar,
  ProgressBar,
  Colors,
  Divider,
} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';

// Icons
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';
import IconIonicons from 'react-native-vector-icons/Ionicons';

import LinearGradient from 'react-native-linear-gradient';
import {URL} from '../../utils/global';
import {name as app_name, version as app_version} from '../../../package.json';

import Appbar from '../../components/appbarHome';
import AppbarT from '../../components/appBarTransparent';
import BottomTab from '../../components/bottomTab';
import Loading from '../../components/loading';
import VersionCheck from 'react-native-version-check';
import {ScrollView} from 'react-native-gesture-handler';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

const wait = timeout => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

function HistoryPage(props) {
  const [refreshing, setRefreshing] = useState(false);
  const [wishlist, setWishlist] = useState(null);
  const [jumlahProdukDitandai, setJumlahProdukDitandai] = useState(null);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [saldo, setSaldo] = useState('0');
  const [totalOrder, setTotalOrder] = useState([]);
  const [jumlahPesanan, setJumlahPesanan] = useState(null);
  const [jumlahKategori, setJumlahKategori] = useState(null);
  const [popup, setPopup] = useState([]);
  const [modal, setModal] = useState(true);
  const [notif, setNotif] = useState(0);

  const {height, width} = Dimensions.get('window');
  const haveProduk = true;
  const urlWishlist = URL + 'v1/wishlist/me';
  const urlCategory = URL + 'v1/category';
  const urlSaldo = URL + 'v1/saldo/my';
  const urlTotalOrder = URL + 'v1/orders/my-order?status=5';
  const urlPopup = URL + 'v1/popup';
  const urlNotif = URL + 'v1/notification/me';

  const iconHistory = 'https://www.linkpicture.com/q/history_-color.png';

  // State Beri App
  const [state, setState] = useState({
    weekly: [
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '14 April 2020',
        time: '14:48',
        amount: 120000,
      },
    ],
    monthly: [
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'January',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'February',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'March',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'April',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'Mei',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'January',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'February',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'March',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'April',
        time: '2020',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: 'Mei',
        time: '2020',
        amount: 120000,
      },
    ],
    yearly: [
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2015',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2016',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2017',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2018',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2019',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2015',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2016',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2017',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2018',
        amount: 120000,
      },
      {
        icon: 'https://www.linkpicture.com/q/history_icon.png',
        title: 'Bantuan pangan un ...',
        date: '2019',
        amount: 120000,
      },
    ],
    isPressWeekly: true,
    isPressMonthly: false,
    isPressYearly: false,
  });

  let pop = 0;
  if (props.route.params.pop != null) {
    pop = props.route.params.pop;
  }

  useEffect(() => {
    getVersion();
    getListWishlist();
    getSaldo();
    getTotalOrder();
    // --- [Deactive] ---
    // getPopup();
    // --- [Deactive] ---
    getNotif();
    getKategori();
  }, []);

  //Pergi ke Hal List Produk
  const listProduk = title => {
    props.navigation.navigate('Produk', {title});
  };

  const getVersion = () => {
    VersionCheck.getLatestVersion().then(latestVersion => {
      console.log(latestVersion); // 0.1.2
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getVersion();
    getListWishlist();
    getSaldo();
    getTotalOrder();
    getPopup();
    getNotif();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  //Pergi ke Hal Pesanan
  const gotoPesanan = () => {
    props.navigation.navigate('PesananSaya', {title: 'Pesanan Saya'});
  };

  const gotoFoundationList = () => {
    props.navigation.navigate('FoundationList', {title: 'Foundation List'});
  };

  //Pergi ke Hal List Kategori
  const gotoKategori = () => {
    props.navigation.navigate('Kategori', {title: 'Produk Lain'});
  };

  //Pergi ke Hal List Wishlist
  const gotoWishlist = () => {
    props.navigation.navigate('WishlistNoButtonTambah', {
      title: 'Tambah Produk Saya',
    });
  };

  const gotoPalingDisukaiSesungguhnya = () => {
    props.navigation.navigate('WishlistSesungguhnya', {
      title: 'Produk yang Ditandai',
    });
  };

  const getNotif = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);
    console.log(data.token);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    console.log(urlNotif + '?order_direction=desc');

    fetch(urlNotif + '?order_direction=desc', {headers})
      .then(response => response.json())
      .then(responseData => {
        let a = 0;
        let data = responseData.data;
        // console.log(responseData.data)
        // data.reverse()
        data.map((val, i) => {
          console.log(val.id + 'status = ' + val.status);
          if (val.status == 0) {
            a++;
          }
        });
        setNotif(a);
      });
  };

  const getKategori = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlCategory + '?limit=9&offset=0', {headers})
      .then(response => response.json())
      .then(responseData => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlCategory', totalData);
        setJumlahKategori(responseData.meta.total_data);
      })
      .catch(e => console.log(e));
  };

  //Untuk Ngecek Berapa saldonya
  const getSaldo = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlSaldo, {headers})
      .then(response => response.json())
      .then(responseData => {
        setLoading(false);
        setSaldo(responseData.data);
      })
      .catch(e => console.log(e));
  };

  //Untuk Ngecek udah ada wishlist apa belum
  const getListWishlist = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlWishlist, {headers})
      .then(response => response.json())
      .then(responseData => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlWishlist', responseData);
        setWishlist(totalData);
        setJumlahProdukDitandai(responseData.meta.total_data);
      })
      .catch(e => console.log(e));
  };

  //Untuk dapetin udah berapa banyak yang order
  const getTotalOrder = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlTotalOrder, {headers})
      .then(response => response.json())
      .then(responseData => {
        console.log('yyyy', responseData);
        setLoading(false);
        setTotalOrder(responseData.data);
        setJumlahPesanan(responseData.meta.total_data);
      })
      .catch(e => console.log(e));
  };

  console.log('sfsdfs', totalOrder);

  //Pergi ke Hal Cari Produk
  const searchProduk = () => {
    props.navigation.navigate('Produk', {title: 'Cari Produk', search: search});
  };

  const getPopup = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlPopup, {headers})
      .then(response => response.json())
      .then(responseData => {
        // console.log(responseData.data)
        setPopup(responseData.data);
        if (pop == 1) setModal(true);
        else {
          setModal(false);
        }
      })
      .catch(e => console.log(e));
  };

  const modalTrigger = async () => {
    setModal(!modal);
  };

  const {
    weekly,
    monthly,
    yearly,
    isPressWeekly,
    isPressMonthly,
    isPressYearly,
  } = state;

  return (
    <View style={{flex: 1, backgroundColor: '#F8F8FB'}}>
      <ImageBackground
        source={require('../../assets/images/banner-home-white.png')}
        style={{justifyContent: 'flex-start', height: height * 0.1}}>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * 0.05,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <Text style={[iOSUIKit.title3Emphasized]}>History</Text>
            <TouchableOpacity onPress={() => alert('Coming Soon Feature')}>
              <IconIonicons
                style={{padding: 10}}
                name="options-outline"
                size={30}
                color={materialColors.blackPrimary}
              />
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>

      <View
        style={{
          width: '90%',
          alignSelf: 'center',
          backgroundColor: '#E9EAEB',
          borderRadius: 12,
          justifyContent: 'space-between',
          alignItems: 'center',
          flexDirection: 'row',
          marginTop: 32,
          padding: 10,
        }}>
        <TouchableOpacity
          onPress={() =>
            setState({
              ...state,
              isPressWeekly: !isPressWeekly,
              isPressMonthly: false,
              isPressYearly: false,
            })
          }>
          <View style={isPressWeekly ? styles.btnPress : styles.btnNomral}>
            <Text
              style={
                isPressWeekly
                  ? iOSUIKit.calloutWhite
                  : [iOSUIKit.callout, {color: materialColors.blackTertiary}]
              }>
              Weekly
            </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            setState({
              ...state,
              isPressMonthly: !isPressMonthly,
              isPressWeekly: false,
              isPressYearly: false,
            })
          }>
          <View style={isPressMonthly ? styles.btnPress : styles.btnNomral}>
            <Text
              style={
                isPressMonthly
                  ? iOSUIKit.calloutWhite
                  : [iOSUIKit.callout, {color: materialColors.blackTertiary}]
              }>
              Monthly
            </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() =>
            setState({
              ...state,
              isPressYearly: !isPressYearly,
              isPressMonthly: false,
              isPressWeekly: false,
            })
          }>
          <View style={isPressYearly ? styles.btnPress : styles.btnNomral}>
            <Text
              style={
                isPressYearly
                  ? iOSUIKit.calloutWhite
                  : [iOSUIKit.callout, {color: materialColors.blackTertiary}]
              }>
              Yearly
            </Text>
          </View>
        </TouchableOpacity>
      </View>

      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginBottom: height * 0.02,
            flex: 1,
          }}>
          <View style={{marginTop: 24}}>
            {isPressWeekly ? (
              weekly.length > 0 ? (
                weekly.map((value, key) => {
                  const {title, date, time, amount, icon} = value;
                  return (
                    <View key={key}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          marginTop: 12,
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <Avatar.Image
                            source={{uri: icon}}
                            size={height * 0.05}
                          />
                          <View>
                            <Text
                              style={[
                                {marginLeft: 12},
                                iOSUIKit.footnoteEmphasized,
                              ]}>
                              {title}
                            </Text>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}>
                              <Text
                                style={[
                                  iOSUIKit.caption2,
                                  {
                                    marginLeft: 12,
                                    marginTop: 4,
                                    color: materialColors.blackTertiary,
                                  },
                                ]}>
                                {date}
                              </Text>
                              <Text
                                style={[
                                  iOSUIKit.caption2,
                                  {
                                    marginLeft: 12,
                                    marginTop: 4,
                                    color: materialColors.blackTertiary,
                                  },
                                ]}>
                                {time}
                              </Text>
                            </View>
                          </View>
                        </View>
                        <View>
                          <Text style={[iOSUIKit.bodyEmphasized]}>
                            Rp {amount}
                          </Text>
                        </View>
                      </View>
                      <Divider style={{marginTop: 14}} />
                    </View>
                  );
                })
              ) : (
                <Text>Loading data ...</Text>
              )
            ) : isPressMonthly ? (
              monthly.length > 0 ? (
                monthly.map((value, key) => {
                  const {title, date, time, amount, icon} = value;
                  return (
                    <View key={key}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          marginTop: 12,
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                          }}>
                          <Avatar.Image
                            source={{uri: icon}}
                            size={height * 0.05}
                          />
                          <View>
                            <Text
                              style={[
                                {marginLeft: 12},
                                iOSUIKit.footnoteEmphasized,
                              ]}>
                              {title}
                            </Text>
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                              }}>
                              <Text
                                style={[
                                  iOSUIKit.caption2,
                                  {
                                    marginLeft: 12,
                                    marginTop: 4,
                                    color: materialColors.blackTertiary,
                                  },
                                ]}>
                                {date}
                              </Text>
                              <Text
                                style={[
                                  iOSUIKit.caption2,
                                  {
                                    marginLeft: 12,
                                    marginTop: 4,
                                    color: materialColors.blackTertiary,
                                  },
                                ]}>
                                {time}
                              </Text>
                            </View>
                          </View>
                        </View>
                        <View>
                          <Text style={[iOSUIKit.bodyEmphasized]}>
                            Rp {amount}
                          </Text>
                        </View>
                      </View>
                      <Divider style={{marginTop: 14}} />
                    </View>
                  );
                })
              ) : (
                <Text>Loading data ...</Text>
              )
            ) : yearly.length > 0 ? (
              yearly.map((value, key) => {
                const {title, date, amount, icon} = value;
                return (
                  <View key={key}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        marginTop: 12,
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <Avatar.Image
                          source={{uri: icon}}
                          size={height * 0.05}
                        />
                        <View>
                          <Text
                            style={[
                              {marginLeft: 12},
                              iOSUIKit.footnoteEmphasized,
                            ]}>
                            {title}
                          </Text>
                          <Text
                            style={[
                              iOSUIKit.caption2,
                              {
                                marginLeft: 12,
                                marginTop: 4,
                                color: materialColors.blackTertiary,
                              },
                            ]}>
                            {date}
                          </Text>
                        </View>
                      </View>
                      <View>
                        <Text style={[iOSUIKit.bodyEmphasized]}>
                          Rp {amount}
                        </Text>
                      </View>
                    </View>
                    <Divider style={{marginTop: 14}} />
                  </View>
                );
              })
            ) : (
              <Text>Loading data ...</Text>
            )}
          </View>
        </View>
      </ScrollView>
      <View style={{position: 'absolute', right: 16, bottom: 66}}>
        <TouchableOpacity
          onPress={() =>
            props.navigation.navigate('DonationReport', {
              title: 'Donation Report',
            })
          }>
          <View
            style={{
              backgroundColor: '#F70161',
              alignSelf: 'flex-end',
              width: height * 0.12,
              height: height * 0.12,
              borderRadius: 50,
              justifyContent: 'center',
            }}>
            <Image
              source={{uri: iconHistory}}
              size={height * 0.1}
              style={{
                width: height * 0.05,
                height: height * 0.05,
                alignSelf: 'center',
              }}
            />
          </View>
        </TouchableOpacity>
      </View>

      {/* {loading && <Loading />} */}

      <BottomTab {...props} />

      {modal
        ? popup.map((data, i) => (
            <View
              key={i}
              style={{
                position: 'absolute',
                flex: 1,
                zIndex: 2,
                width: width,
                height: height,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.shadow,
                  {
                    alignSelf: 'center',
                    width: width * 0.6,
                    backgroundColor: 'rgba(255,255,255,1)',
                    padding: 15,
                  },
                ]}>
                <Text style={{textAlign: 'center', marginBottom: 10}}>
                  {data.name}
                </Text>
                <Image
                  source={{uri: data.image_url}}
                  style={{
                    width: '80%',
                    alignSelf: 'center',
                    height: height * 0.3,
                    resizeMode: 'contain',
                  }}
                />

                <TouchableOpacity
                  style={{alignSelf: 'flex-end'}}
                  onPress={() => modalTrigger()}>
                  <Text style={{fontSize: 14, color: '#07A9F0'}}>Tutup</Text>
                </TouchableOpacity>
              </View>
            </View>
          ))
        : null}
    </View>
  );
}

export default HistoryPage;

const styles = StyleSheet.create({
  btnPress: {
    padding: 15,
    borderRadius: 13,
    backgroundColor: '#F70161',
  },
  btnNomral: {
    padding: 15,
    borderRadius: 12,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
