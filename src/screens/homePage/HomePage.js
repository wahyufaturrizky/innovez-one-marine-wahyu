/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import {
  Text,
  Card,
  Chip,
  Avatar,
  ProgressBar,
  Colors,
  Checkbox,
} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';
import {URL} from '../../utils/global';
import {name as app_name, version as app_version} from '../../../package.json';

import Appbar from '../../components/appbarHome';
import AppbarT from '../../components/appBarTransparent';
import BottomTab from '../../components/bottomTab';
import Loading from '../../components/loading';
import VersionCheck from 'react-native-version-check';
import {ScrollView} from 'react-native-gesture-handler';

// React Native MQTT
import MQTTConnection from './MQTTConnection';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

// Buffer
import {Buffer} from 'buffer';

global.Buffer = Buffer;

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function HomePage(props) {
  const [refreshing, setRefreshing] = useState(false);
  const [loading, setLoading] = useState(true);

  const {height, width} = Dimensions.get('window');

  useEffect(() => {
    mqttConnect = new MQTTConnection();
    mqttConnect.onMQTTConnect = onMQTTConnect;
    mqttConnect.onMQTTLost = onMQTTLost;
    mqttConnect.onMQTTMessageArrived = onMQTTMessageArrived;
    mqttConnect.onMQTTMessageDelivered = onMQTTMessageDelivered;

    mqttConnect.connect('broker.mqttdashboard.com', 8000);

    onMQTTConnect = () => {
      console.log('App onMQTTConnect');
      mqttConnect.subscribeChannel('hanth2');
    };

    onMQTTLost = () => {
      console.log('App onMQTTLost');
    };

    onMQTTMessageArrived = (message) => {
      // console.log('App onMQTTMessageArrived: ', message);
      console.log(
        'App onMQTTMessageArrived payloadString: ',
        message.payloadString,
      );
    };

    onMQTTMessageDelivered = (message) => {
      console.log('App onMQTTMessageDelivered: ', message.payloadString);
    };

    return () => {
      mqttConnect.close();
    };
  }, []);

  // State Mqrine App

  const [checked, setChecked] = React.useState(false);

  const [state, setState] = useState({
    news: [
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
      {
        title:
          'Beberapa influencer siapkan acara galang dana untuk korban covid',
        like: 34,
        sub_title: 'In this modern day bussiness is more...',
        imageUrl:
          'https://images.unsplash.com/photo-1603843722974-3a4031f9f97c?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTZ8fGNvdmlkJTIwMTl8ZW58MHx8MHw%3D&auto=format&fit=crop&w=500&q=60',
      },
    ],
  });

  const {news} = state;

  const getVersion = () => {
    VersionCheck.getLatestVersion().then((latestVersion) => {});
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getVersion();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  const gotoLihatDetailsCampaign = () => {
    props.navigation.navigate('DetailsCampaign', {title: 'Detail Campaign'});
  };

  return (
    <View style={{flex: 1, backgroundColor: '#F8F8FB'}}>
      <Appbar params={props} />
      {/* <ImageBackground
        source={require('../../assets/images/banner-home-white.png')}
        style={{justifyContent: 'flex-start', height: height * 0.11}}>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * 0.03,
          }}>
          <Text style={[iOSUIKit.subhead, {marginVertical: 5}]}>
            Halo,{' '}
            <Text style={[systemWeights.semibold]}>Wahyu Fatur Rizki</Text>
          </Text>
          <Text
            style={[
              iOSUIKit.title3Emphasized,
              {marginVertical: 5, fontWeight: '700'},
            ]}>
            Launch Job
          </Text>
        </View>
      </ImageBackground> */}
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginTop: height * 0.01,
            flex: 1,
          }}>
          {news.map((value, key) => {
            const {imageUrl, title, sub_title, like} = value;
            return (
              <TouchableOpacity
                key={key}
                onPress={() => gotoLihatDetailsCampaign()}>
                <View
                  style={{
                    marginTop: height * 0.02,
                    backgroundColor: '#0080E0',
                    borderTopRightRadius: 12,
                    borderTopLeftRadius: 12,
                    padding: 15,
                  }}>
                  <View
                    style={{
                      marginTop: 8,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <View>
                      <Text style={[iOSUIKit.calloutWhite]}>WCP</Text>
                      <Text style={[iOSUIKit.calloutWhite, {marginTop: 8}]}>
                        Manifest: 2 PAX
                      </Text>
                    </View>
                    <IconFeather
                      name="arrow-right"
                      size={width * 0.07}
                      color="#fff"
                    />
                    <View>
                      <Text style={[iOSUIKit.calloutWhite]}>APOLO ACE</Text>
                      <Text
                        style={[
                          iOSUIKit.calloutWhite,
                          {marginTop: 8, textAlign: 'right'},
                        ]}>
                        AWPA
                      </Text>
                      <Text
                        style={[
                          iOSUIKit.calloutWhite,
                          {marginTop: 8, textAlign: 'right'},
                        ]}>
                        4210D
                      </Text>
                    </View>
                  </View>
                </View>
                <View
                  style={{
                    backgroundColor: '#fff',
                    borderBottomRightRadius: 12,
                    borderBottomLeftRadius: 12,
                    padding: 15,
                  }}>
                  <View
                    style={{
                      marginTop: 8,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      paddingBottom: 8,
                      borderBottomWidth: 1,
                      borderColor: '#C4C4C4',
                    }}>
                    <View>
                      <Checkbox
                        status={checked ? 'checked' : 'unchecked'}
                        onPress={() => {
                          setChecked(!checked);
                        }}
                      />
                    </View>
                    <View>
                      <Text style={[iOSUIKit.callout]}>From</Text>
                    </View>
                    <View>
                      <Text style={[iOSUIKit.callout]}>12:00</Text>
                    </View>
                    <View>
                      <Text style={[iOSUIKit.callout]}>Pick</Text>
                    </View>
                  </View>
                  <View
                    style={{
                      marginTop: 8,

                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                    }}>
                    <View>
                      <Checkbox
                        status={checked ? 'checked' : 'unchecked'}
                        onPress={() => {
                          setChecked(!checked);
                        }}
                      />
                    </View>
                    <View>
                      <Text style={[iOSUIKit.callout]}>To</Text>
                    </View>
                    <View>
                      <Text style={[iOSUIKit.callout]}>14:00</Text>
                    </View>
                    <View>
                      <Text style={[iOSUIKit.callout]}>Send</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
      {/* {loading && <Loading />} */}

      {/* ----- [START BUTTON BOTTOM OF PAGE] ----- */}
      <TouchableOpacity
        onPress={() =>
          mqttConnect.send('hanth2', 'message send to channel hanth2 again')
        }>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          colors={['#0080E0', '#02AAEC', '#67CCF4']}
          style={{padding: 15, margin: 15, borderRadius: 12}}>
          <Text style={{fontSize: 24, textAlign: 'center', color: 'white'}}>
            START
          </Text>
        </LinearGradient>
      </TouchableOpacity>
      {/* ----- [END BUTTON BOTTOM OF PAGE] ----- */}

      <BottomTab {...props} />
    </View>
  );
}

export default HomePage;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
