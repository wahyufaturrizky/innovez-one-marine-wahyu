import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  Modal,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {CommonActions} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';

// React Native Paper Component
import {TextInput} from 'react-native-paper';

// Custome Phone Input by Country Code
import PhoneInput from 'react-native-phone-input';

// Typography
import {material, systemWeights, materialColors} from 'react-native-typography';

export default class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      modalVisibleRegister: false,
      username: '',
      password: '',
    };
  }
  componentDidMount = () => {
    SplashScreen.hide();
    this.checkLogin();
  };

  onChangeText = (value, name) => {
    this.setState({
      [name]: value,
    });
  };

  goToHome = () => {
    this.props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [
          {
            name: 'HomePage',
            params: {
              title: 'Jualan Anda',
              pop: 1,
            },
          },
        ],
      }),
    );
  };

  //Proses Login
  SignIn = async () => {
    this.goToHome();
    this.setState({
      modalVisible: false,
    });
    // setLoading(true);
    // let formdata = new FormData();
    // formdata.append('username', 'a@a.com');
    // formdata.append('password', 'a');

    // let requestOptions = {
    //   method: 'POST',
    //   body: formdata,
    //   redirect: 'follow',
    //   headers: {
    //     Accept: '*/*',
    //     'Content-Type': 'multipart/form-data',
    //   },
    // };

    // fetch(urlLogin, requestOptions)
    //   .then(response => response.json())
    //   .then(async responseData => {
    //     setLoading(false);
    //     if (responseData.data == 'Password incorrect') {
    //       alert('Password Anda Salah');
    //     } else if (responseData.data == 'Your account not yet registered') {
    //       alert('Akun Belum Terdaftar');
    //     } else {
    //       await AsyncStorage.setItem('data', JSON.stringify(responseData.data));
    //       console.log(responseData.data);
    //       goToHome();
    //     }
    //   })
    //   .done();
  };

  checkLogin = async () => {
    let value = await AsyncStorage.getItem('data');
    if (value != null) {
      console.log(value);
      this.goToHome();
    }
    console.log('checkLogin', value);
  };

  handleRegular = async () => {
    await AsyncStorage.setItem('regular', 'true');
    this.setState({modalVisible: false});
    this.props.navigation.navigate('Login');
  };

  render() {
    const {modalVisible, modalVisibleRegister, name} = this.state;
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.backgroundImageStyle}
          source={require('../../assets/images/background_image_login.jpg')}>
          <Image
            resizeMode="contain"
            source={require('../../assets/images/logo_horizontal_beri.png')}
          />
          {/* ------ [START BUTTON CHOICE] ------ */}
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.setState({modalVisible: true});
            }}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={['#FFFFFF', '#FFFFFF', '#FFFFFF']}
              style={styles.button}>
              <Text style={[styles.buttonText, styles.buttonTextColor]}>
                Login
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          {/* <TouchableOpacity
            style={styles.button}
            onPress={() => {
              this.setState({modalVisibleRegister: true});
            }}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={['#FFFFFF00', '#FFFFFF00', '#FFFFFF00']}
              style={[styles.button, styles.buttonWithBorderColor]}>
              <Text style={[styles.buttonText, styles.buttonTextColorWhite]}>
                Register
              </Text>
            </LinearGradient>
          </TouchableOpacity> */}
          {/* ------ [START BUTTON CHOICE] ------ */}
          {/* ------ [] ------ */}
          {/* ------ [START MODAL LOGIN] ------ */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              // Alert.alert('Modal has been closed.');
              // console.log('Modal has been closed');
              this.setState({
                modalVisible: false,
              });
            }}>
            <View style={styles.centeredView}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    modalVisible: false,
                  });
                }}
                style={{position: 'absolute', top: 20, left: 20}}>
                <Image
                  style={{
                    borderColor: '#16212D',
                    borderWidth: 1,
                    borderRadius: 10,
                    backgroundColor: 'white',
                  }}
                  source={require('../../assets/images/close.png')}
                />
              </TouchableOpacity>
              <View style={styles.modalView}>
                <Text style={[styles.modalText, material.title]}>
                  Welcome Back!
                </Text>
                <Text
                  style={[
                    styles.modalText,
                    material.subheading,
                    {
                      color: materialColors.blackTertiary,
                    },
                  ]}>
                  Enter your username and password to login.
                </Text>

                <View
                  style={[
                    styles.shadow,
                    {
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#fff',
                      borderRadius: 10,
                      bottom: 10,
                      width: '100%',
                      alignSelf: 'center',
                      marginBottom: 30,
                    },
                  ]}>
                  <Icon
                    style={{padding: 10}}
                    name="account"
                    size={20}
                    color="#949494"
                  />
                  <TextInput
                    style={{
                      flex: 1,
                      paddingTop: 0,
                      paddingRight: 0,
                      paddingBottom: 0,
                      paddingLeft: 0,
                      backgroundColor: '#fff',
                      height: 50,
                    }}
                    onChangeText={(val) => this.onChangeText(val, 'username')}
                    placeholder="Input username"
                    underlineColorAndroid="transparent"
                    onSubmitEditing={
                      () => alert('Under Constractions')
                      // searchProduk
                    }
                  />
                </View>
                <View
                  style={[
                    styles.shadow,
                    {
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: '#fff',
                      borderRadius: 10,
                      bottom: 10,
                      width: '100%',
                      alignSelf: 'center',
                      marginBottom: 30,
                    },
                  ]}>
                  <Icon
                    style={{padding: 10}}
                    name="lock"
                    size={20}
                    color="#949494"
                  />
                  <TextInput
                    style={{
                      flex: 1,
                      paddingTop: 0,
                      paddingRight: 0,
                      paddingBottom: 0,
                      paddingLeft: 0,
                      backgroundColor: '#fff',
                      height: 50,
                    }}
                    secureTextEntry={true}
                    onChangeText={(val) => this.onChangeText(val, 'password')}
                    placeholder="Input password"
                    underlineColorAndroid="transparent"
                    onSubmitEditing={
                      () => alert('Under Constractions')
                      // searchProduk
                    }
                  />
                </View>

                <TouchableOpacity
                  onPress={() => {
                    this.SignIn();
                  }}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                    colors={['#0080E0', '#02AAEC', '#67CCF4']}
                    style={styles.openButton}>
                    <Text
                      style={[styles.buttonText, styles.buttonTextColorWhite]}>
                      Login
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>

                {/* <Text
                  style={[
                    material.caption,
                    {textAlign: 'center', marginBottom: 20},
                  ]}>
                  OR
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginBottom: 20,
                  }}>
                  <TouchableOpacity
                    onPress={() => alert('Coming Soon Feature')}>
                    <Image
                      style={{width: 30, height: 30}}
                      source={require('../../assets/images/Google.png')}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => alert('Coming Soon Feature')}>
                    <Image
                      style={{width: 30, height: 30}}
                      source={require('../../assets/images/Facebook_02.png')}
                    />
                  </TouchableOpacity>
                </View>

                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('LoginOtp', {
                      title: 'Login OTP',
                    });
                    this.setState({modalVisible: false});
                  }}>
                  <Text
                    style={[
                      {
                        textAlign: 'center',
                        color: materialColors.blackTertiary,
                      },
                      material.caption,
                    ]}>
                    Can't login?
                    <Text style={{color: materialColors.blackPrimary}}>
                      {' '}
                      Send OTP
                    </Text>
                  </Text>
                </TouchableOpacity> */}
              </View>
            </View>
          </Modal>
          {/* ------ [END MODAL LOGIN] ------ */}
          {/* ------ [] ------ */}
          {/* ------ [START MODAL REGISTER] ------ */}
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisibleRegister}
            onRequestClose={() => {
              // Alert.alert('Modal has been closed.');
              this.setState({
                modalVisibleRegister: false,
              });
            }}>
            <View style={styles.centeredViewRegister}>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    modalVisibleRegister: false,
                  });
                }}
                style={{position: 'absolute', left: 20, top: 20}}>
                <Image
                  style={{
                    borderColor: '#16212D',
                    borderWidth: 1,
                    borderRadius: 10,
                    backgroundColor: 'white',
                  }}
                  source={require('../../assets/images/close.png')}
                />
              </TouchableOpacity>
              <View style={styles.modalView}>
                <Text style={[styles.modalText, material.title]}>
                  Get Started!
                </Text>
                <Text
                  style={[
                    styles.modalText,
                    material.subheading,
                    {
                      color: materialColors.blackTertiary,
                    },
                  ]}>
                  Let's help others and start by create your account.
                </Text>

                <TextInput
                  label="Name"
                  value={name}
                  theme={{
                    colors: {
                      primary: '#F70161',
                      underlineColor: 'transparent',
                    },
                  }}
                  onChangeText={(text) => this.onChangeText(text, 'name')}
                  mode="outlined"
                  style={{
                    marginBottom: 80,
                  }}
                />

                <PhoneInput
                  initialCountry="id"
                  autoFormat={true}
                  style={{
                    marginBottom: 20,
                    paddingBottom: 10,
                    borderColor: '#F70161',
                    borderBottomWidth: 1,
                  }}
                  ref={(ref) => {
                    this.phone = ref;
                  }}
                />

                <TouchableOpacity
                  style={{
                    ...styles.openButton,
                    backgroundColor: '#F70161',
                  }}
                  // onPress={() => this.props.navigation.navigate('Login')}
                  onPress={() => alert('under construction')}>
                  <Text style={[styles.textStyle]}>Create Account</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          {/* ------ [END MODAL REGISTER] ------ */}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  centeredViewRegister: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  modalView: {
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    padding: 35,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 15,
    elevation: 2,
    marginBottom: 20,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 30,
    textAlign: 'left',
  },
  backgroundImageStyle: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // justifyContent: 'center',
    // alignItems: 'center',
    flexDirection: 'column',
  },
  button: {
    borderRadius: 20,
    width: '90%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  buttonWithBorderColor: {
    borderWidth: 2,
    borderColor: 'white',
  },
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
  },
  buttonTextColor: {
    color: '#0080E0',
  },
  buttonTextColorWhite: {
    color: '#FFFFFF',
  },
});
