/* eslint-disable */
import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import {
  Text,
  Card,
  Chip,
  Avatar,
  ProgressBar,
  Colors,
} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';

// Icons
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import IconIonicons from 'react-native-vector-icons/Ionicons';

import LinearGradient from 'react-native-linear-gradient';
import {URL} from '../../utils/global';
import {name as app_name, version as app_version} from '../../../package.json';

import Appbar from '../../components/appbarHome';
import AppbarT from '../../components/appBarTransparent';
import BottomTab from '../../components/bottomTab';
import Loading from '../../components/loading';
import VersionCheck from 'react-native-version-check';
import {ScrollView} from 'react-native-gesture-handler';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

const wait = timeout => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

function FoundationList(props) {
  const [refreshing, setRefreshing] = useState(false);
  const [wishlist, setWishlist] = useState(null);
  const [jumlahProdukDitandai, setJumlahProdukDitandai] = useState(null);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [saldo, setSaldo] = useState('0');
  const [totalOrder, setTotalOrder] = useState([]);
  const [jumlahPesanan, setJumlahPesanan] = useState(null);
  const [jumlahKategori, setJumlahKategori] = useState(null);
  const [popup, setPopup] = useState([]);
  const [modal, setModal] = useState(true);
  const [notif, setNotif] = useState(0);

  const {height, width} = Dimensions.get('window');
  const haveProduk = true;
  const urlWishlist = URL + 'v1/wishlist/me';
  const urlCategory = URL + 'v1/category';
  const urlSaldo = URL + 'v1/saldo/my';
  const urlTotalOrder = URL + 'v1/orders/my-order?status=5';
  const urlPopup = URL + 'v1/popup';
  const urlNotif = URL + 'v1/notification/me';

  // State Beri App
  const [state, setState] = useState({
    foundationListData: [
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
      {
        imageUrl:
          'https://images.unsplash.com/photo-1605722558200-b83d78c255a7?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxzZWFyY2h8MTF8fGNvdmlkfGVufDB8fDB8&auto=format&fit=crop&w=500&q=60',
        issuer: 'Wahyu Fatur Rizki',
        title: 'Aksi cepat tanggap darurat bencana di lombok',
        subtitle: 'Galang dana untuk korban benca ...',
      },
    ],
  });

  let pop = 0;
  if (props.route.params.pop != null) {
    pop = props.route.params.pop;
  }

  useEffect(() => {
    getVersion();
    getListWishlist();
    getSaldo();
    getTotalOrder();
    // --- [Deactive] ---
    // getPopup();
    // --- [Deactive] ---
    getNotif();
    getKategori();
  }, []);

  //Pergi ke Hal List Produk
  const listProduk = title => {
    props.navigation.navigate('Produk', {title});
  };

  const getVersion = () => {
    VersionCheck.getLatestVersion().then(latestVersion => {
      console.log(latestVersion); // 0.1.2
    });
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getVersion();
    getListWishlist();
    getSaldo();
    getTotalOrder();
    getPopup();
    getNotif();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  //Pergi ke Hal Pesanan
  const gotoPesanan = () => {
    props.navigation.navigate('PesananSaya', {title: 'Pesanan Saya'});
  };

  //Pergi ke Hal List Kategori
  const gotoKategori = () => {
    props.navigation.navigate('Kategori', {title: 'Produk Lain'});
  };

  //Pergi ke Hal List Wishlist
  const gotoWishlist = () => {
    props.navigation.navigate('WishlistNoButtonTambah', {
      title: 'Tambah Produk Saya',
    });
  };

  const gotoPalingDisukaiSesungguhnya = () => {
    props.navigation.navigate('WishlistSesungguhnya', {
      title: 'Produk yang Ditandai',
    });
  };

  const getNotif = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);
    console.log(data.token);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    console.log(urlNotif + '?order_direction=desc');

    fetch(urlNotif + '?order_direction=desc', {headers})
      .then(response => response.json())
      .then(responseData => {
        let a = 0;
        let data = responseData.data;
        // console.log(responseData.data)
        // data.reverse()
        data.map((val, i) => {
          console.log(val.id + 'status = ' + val.status);
          if (val.status == 0) {
            a++;
          }
        });
        setNotif(a);
      });
  };

  const getKategori = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlCategory + '?limit=9&offset=0', {headers})
      .then(response => response.json())
      .then(responseData => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlCategory', totalData);
        setJumlahKategori(responseData.meta.total_data);
      })
      .catch(e => console.log(e));
  };

  //Untuk Ngecek Berapa saldonya
  const getSaldo = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlSaldo, {headers})
      .then(response => response.json())
      .then(responseData => {
        setLoading(false);
        setSaldo(responseData.data);
      })
      .catch(e => console.log(e));
  };

  //Untuk Ngecek udah ada wishlist apa belum
  const getListWishlist = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlWishlist, {headers})
      .then(response => response.json())
      .then(responseData => {
        setLoading(false);
        let totalData = responseData.data.length;
        // console.log('urlWishlist', responseData);
        setWishlist(totalData);
        setJumlahProdukDitandai(responseData.meta.total_data);
      })
      .catch(e => console.log(e));
  };

  //Untuk dapetin udah berapa banyak yang order
  const getTotalOrder = async () => {
    setLoading(true);
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlTotalOrder, {headers})
      .then(response => response.json())
      .then(responseData => {
        console.log('yyyy', responseData);
        setLoading(false);
        setTotalOrder(responseData.data);
        setJumlahPesanan(responseData.meta.total_data);
      })
      .catch(e => console.log(e));
  };

  console.log('sfsdfs', totalOrder);

  //Pergi ke Hal Cari Produk
  const searchProduk = () => {
    props.navigation.navigate('Produk', {title: 'Cari Produk', search: search});
  };

  const getPopup = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlPopup, {headers})
      .then(response => response.json())
      .then(responseData => {
        // console.log(responseData.data)
        setPopup(responseData.data);
        if (pop == 1) setModal(true);
        else {
          setModal(false);
        }
      })
      .catch(e => console.log(e));
  };

  const modalTrigger = async () => {
    setModal(!modal);
  };

  const {
    categoryDonate,
    thumbDonate,
    news,
    categoryList,
    foundationListData,
  } = state;

  return (
    <View style={{flex: 1, backgroundColor: '#F8F8FB'}}>
      <Appbar params={props} />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          bottom: 10,
          width: '70%',
          alignSelf: 'center',
          marginTop:20,
        }}>
        <View
          style={[
            styles.shadow,
            {
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#fff',
              borderRadius: 10,
            },
          ]}>
          <Icon
            style={{padding: 10}}
            name="magnify"
            size={20}
            color="#949494"
          />
          <TextInput
            style={{
              flex: 1,
              paddingTop: 10,
              paddingRight: 10,
              paddingBottom: 10,
              paddingLeft: 0,
              color: '#424242',
              height: 50,
            }}
            onChangeText={val => setSearch(val)}
            placeholder="Masukkan kata kunci foundation"
            underlineColorAndroid="transparent"
            onSubmitEditing={
              () => alert('Under Constractions')
              // searchProduk
            }
          />
        </View>
        <TouchableOpacity onPress={() => alert('Coming Soon Feature')}>
          <IconIonicons
            style={{
              padding: 10,
              backgroundColor: '#d6245a',
              borderRadius: 12,
              marginLeft: 10,
            }}
            name="options-outline"
            size={30}
            color="white"
          />
        </TouchableOpacity>
      </View>

      <ScrollView
        keyboardShouldPersistTaps="always"
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginBottom: height * 0.02,
            flex: 1,
          }}>
          {foundationListData.map((value, key) => {
            const {imageUrl, title, subtitle, issuer} = value;
            return (
              <View
                key={key}
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: height * 0.01,

                  backgroundColor: 'white',
                  borderRadius: 12,
                  padding: 15,
                }}>
                <View style={{width: '32%'}}>
                  <Image
                    source={{uri: imageUrl}}
                    resizeMode="cover"
                    style={{
                      height: height * 0.15,
                      borderRadius: 12,
                    }}
                  />
                </View>

                <View style={{width: '64%'}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}>
                    <Text
                      style={{
                        fontSize: 10,
                        color: materialColors.blackTertiary,
                        fontWeight: 'bold',
                      }}>
                      {issuer}
                    </Text>
                    <FontAwesomeIcon
                      name="check-circle"
                      size={width * 0.03}
                      color={materialColors.blackPrimary}
                      style={{marginLeft: 8}}
                    />
                  </View>
                  <View
                    style={{
                      justifyContent: 'flex-start',
                      marginTop: 10,
                      height: height * 0.18,
                    }}>
                    <Text
                      style={{
                        color: 'black',
                        fontSize: width * 0.04,
                        marginLeft: width * 0.01,
                        fontWeight: 'bold',
                      }}>
                      {title}
                    </Text>
                    <Text
                      style={{
                        color: materialColors.blackTertiary,
                        fontSize: width * 0.03,
                        marginLeft: width * 0.01,
                        marginTop: 10,
                      }}>
                      {subtitle}
                    </Text>
                    <TouchableOpacity
                      onPress={() =>
                        props.navigation.navigate('FoundationDetails', {
                          title: 'Foundation Details',
                        })  
                      }
                      style={{
                        alignSelf:"flex-end",
                        width:"70%"
                      }}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 1}}
                        colors={['white', 'white', 'white']}
                        style={{
                          marginTop: 10,
                          padding: 15,
                          borderRadius: 14,
                          borderColor: materialColors.blackPrimary,
                          borderWidth: 1,
                        }}>
                        <Text
                          style={[
                            iOSUIKit.footnoteEmphasized,
                            {
                              textAlign: 'center',
                              color: materialColors.blackPrimary,
                              fontWeight: "700"
                            },
                          ]}>
                          Donasi Langsung
                        </Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            );
          })}
        </View>
      </ScrollView>
      {/* {loading && <Loading />} */}

      <BottomTab {...props} />

      {modal
        ? popup.map((data, i) => (
            <View
              key={i}
              style={{
                position: 'absolute',
                flex: 1,
                zIndex: 2,
                width: width,
                height: height,
                bottom: 0,
                backgroundColor: 'rgba(0,0,0,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.shadow,
                  {
                    alignSelf: 'center',
                    width: width * 0.6,
                    backgroundColor: 'rgba(255,255,255,1)',
                    padding: 15,
                  },
                ]}>
                <Text style={{textAlign: 'center', marginBottom: 10}}>
                  {data.name}
                </Text>
                <Image
                  source={{uri: data.image_url}}
                  style={{
                    width: '80%',
                    alignSelf: 'center',
                    height: height * 0.3,
                    resizeMode: 'contain',
                  }}
                />

                <TouchableOpacity
                  style={{alignSelf: 'flex-end'}}
                  onPress={() => modalTrigger()}>
                  <Text style={{fontSize: 14, color: '#07A9F0'}}>Tutup</Text>
                </TouchableOpacity>
              </View>
            </View>
          ))
        : null}
    </View>
  );
}

export default FoundationList;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
  shadowModal: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
