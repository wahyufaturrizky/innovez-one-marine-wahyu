import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  ImageBackground,
  RefreshControl,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

import {TextInput} from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import {listenToKeyboardEvents} from 'react-native-keyboard-aware-scroll-view';

import {URL} from '../../utils/global';
import Loading from '../../components/loading';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

// Icons
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';

const KeyboardAwareScrollView = listenToKeyboardEvents((props) => (
  <ScrollView {...props} />
));

function Login(props) {
  const [refreshing, setRefreshing] = useState(false);
  const [fullname, setFullname] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [login, setLogin] = useState(true);
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);

  const logo = '../../assets/images/logo.png';
  const indonesia = '../../assets/images/indonesia.png';

  const {height, width} = Dimensions.get('window');

  const urlRegister = URL + 'v1/oauth/register';
  const urlLogin = URL + 'v1/oauth/login';

  useEffect(() => {
    checkLogin();
  }, []);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    getVersion();
    getListWishlist();
    getSaldo();
    getTotalOrder();
    getPopup();
    getNotif();
    wait(2000).then(() => setRefreshing(false));
  }, []);

  // Pergi Ke Home Reset Index
  const goToHome = () => {
    props.navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'HomePage', params: {title: 'Jualan Anda', pop: 1}}],
      }),
    );
  };

  //Check Udah ada data di local storage apa belum
  const checkLogin = async () => {
    let value = await AsyncStorage.getItem('data');
    if (value != null) {
      console.log(value);
      goToHome();
    }
    console.log('coba dilihat', value);
  };

  //Modal Lupa Password
  const forgotTrigger = () => {
    setModal(!modal);
  };

  //Proses Login
  const SignIn = async () => {
    goToHome();
    // setLoading(true);
    // let formdata = new FormData();
    // formdata.append('username', 'a@a.com');
    // formdata.append('password', 'a');

    // let requestOptions = {
    //   method: 'POST',
    //   body: formdata,
    //   redirect: 'follow',
    //   headers: {
    //     Accept: '*/*',
    //     'Content-Type': 'multipart/form-data',
    //   },
    // };

    // fetch(urlLogin, requestOptions)
    //   .then(response => response.json())
    //   .then(async responseData => {
    //     setLoading(false);
    //     if (responseData.data == 'Password incorrect') {
    //       alert('Password Anda Salah');
    //     } else if (responseData.data == 'Your account not yet registered') {
    //       alert('Akun Belum Terdaftar');
    //     } else {
    //       await AsyncStorage.setItem('data', JSON.stringify(responseData.data));
    //       console.log(responseData.data);
    //       goToHome();
    //     }
    //   })
    //   .done();
  };

  //Proses Daftar
  const SignUp = () => {
    setLoading(true);
    let formdata = new FormData();
    formdata.append('fullname', fullname);
    formdata.append('phone', phone);
    formdata.append('password', password);
    formdata.append('email', email);
    formdata.append('username', email);

    let requestOptions = {
      method: 'POST',
      body: formdata,
      redirect: 'follow',
      headers: {
        Accept: '*/*',
        'Content-Type': 'multipart/form-data',
      },
    };

    fetch(urlRegister, requestOptions)
      .then((response) => response.json())
      .then(async (responseData) => {
        setLoading(false);
        if (!responseData.status) {
          if (responseData.data.email != null) {
            alert('Email Sudah Terdaftar');
          } else if (responseData.data.phone != null) {
            alert('Nomor Handphone Sudah Terdaftar');
          }
        } else if (responseData.status) {
          await AsyncStorage.setItem('data', JSON.stringify(responseData.data));
          alert('Pendaftaran Berhasil');
          goToHome();
        }
      })
      .done();
  };

  const changeLogin = () => {
    setLogin(!login);
  };

  //Submit Lupa Password
  const forgotSubmit = () => {
    alert('tes');
  };

  // State Beri App
  const [state, setState] = useState({
    categoryList: [
      {
        title: 'Poverty',
      },
      {
        title: 'Health',
      },
      {
        title: 'Education',
      },
      {
        title: 'Equality',
      },
      {
        title: 'Animals',
      },
      {
        title: 'Environment',
      },
      {
        title: 'Music',
      },
    ],
  });

  const {categoryList} = state;

  return (
    <View style={{backgroundColor: '#F8F8FB', flex: 1}}>
      <ImageBackground
        source={require('../../assets/images/banner-home-white.png')}
        style={{justifyContent: 'flex-start', height: height * 0.15}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            width: '90%',
            alignSelf: 'center',
          }}>
          <View
            style={{
              width: '70%',
              marginTop: height * 0.04,
            }}>
            <Text
              style={[
                iOSUIKit.bodyEmphasized,
                {
                  fontWeight: '700',
                },
              ]}>
              Pilih 5 Kategori Donasi Favoritmu
            </Text>
          </View>
          <View
            style={{
              width: '30%',
              marginTop: height * 0.04,
              alignItems: 'flex-end',
            }}>
            <TouchableOpacity style={styles.button} onPress={SignIn}>
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 1}}
                colors={['#d6245a', '#d6245a', '#d6245a']}
                style={styles.button}>
                <Text style={[styles.textStyle, iOSUIKit.footnoteWhite]}>
                  Lanjut
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>

      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            flexDirection: 'row',
            flexWrap: 'wrap',
            marginTop: height * 0.02,
            alignItems: 'flex-start',
            flex: 1,
          }}>
          {categoryList.length > 0 ? (
            categoryList.map((value, key) => (
              <TouchableOpacity
                key={key}
                onPress={() => {
                  alert('Under Construction');
                  // gotoPesanan()
                }}
                style={{width: '50%'}}>
                {console.log('value', value.icon)}
                <ImageBackground
                  imageStyle={{borderRadius: 6, opacity: 0.85}}
                  source={require('../../assets/images/thumb-donate-black.png')}
                  resizeMode="stretch"
                  style={{
                    // justifyContent: 'flex-end',
                    margin: 10,
                    height: height * 0.3,
                    // backgroundColor: 'white',
                  }}>
                  <Image
                    source={require('../../assets/images/oncheck.png')}
                    style={{
                      width: width * 0.07,
                      height: width * 0.07,
                      alignSelf: 'flex-end',
                      marginRight: width * 0.03,
                      marginTop: width * 0.03,
                      resizeMode: 'contain',
                    }}
                  />
                  <Text
                    style={[
                      iOSUIKit.calloutWhite,
                      {
                        textAlign: 'left',
                        marginLeft: 15,
                        marginTop: width * 0.4,
                        fontWeight: 'bold',
                      },
                    ]}>
                    {value.title}
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
            ))
          ) : (
            <Text style={iOSUIKit.caption}>Loading data ...</Text>
          )}
        </View>
      </ScrollView>

      {/* ---- [DEACTIVE LOGIN] ---- */}
      {/* <KeyboardAwareScrollView>
        <View>
          <Image
            source={require(logo)}
            style={{
              width: width * 1.2,
              height: height * 0.3,
              alignSelf: 'center',
              marginBottom: height * 0.05,
              marginTop: height * 0.08,
            }}
            resizeMode="cover"
            width={width * 1.2}
            height={height * 0.3}
          />

          <View style={{flex: 1}}>
            {!login && (
              <TextInput
                label="Nama Lengkap"
                value={fullname}
                mode="outlined"
                onChangeText={val => setFullname(val)}
                selectionColor={'#07A9F0'}
                underlineColor={'#07A9F0'}
                underlineColorAndroid={'#07A9F0'}
                theme={{
                  colors: {primary: '#07A9F0', underlineColor: 'transparent'},
                }}
                style={{
                  width: '90%',
                  alignSelf: 'center',
                  backgroundColor: 'white',
                  marginBottom: height * 0.01,
                }}
              />
            )}

            <TextInput
              label="Email"
              value={email}
              mode="outlined"
              keyboardType="email-address"
              onChangeText={val => setEmail(val)}
              selectionColor={'#07A9F0'}
              underlineColor={'#07A9F0'}
              underlineColorAndroid={'#07A9F0'}
              theme={{
                colors: {primary: '#07A9F0', underlineColor: 'transparent'},
              }}
              style={{
                width: '90%',
                alignSelf: 'center',
                backgroundColor: 'white',
                marginBottom: height * 0.01,
              }}
            />

            {!login && (
              <View
                style={{
                  width: '90%',
                  alignSelf: 'center',
                  justifyContent: 'space-between',
                  flexDirection: 'row',
                  marginBottom: height * 0.01,
                }}>
                <View
                  style={{
                    height: 55,
                    width: '25%',
                    marginTop: 7,
                    borderRadius: 5,
                    borderColor: 'grey',
                    borderWidth: 1,
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      flex: 1,
                    }}>
                    <Image
                      source={require(indonesia)}
                      style={{
                        width: width * 0.06,
                        height: height * 0.03,
                        alignSelf: 'center',
                      }}
                      resizeMode="cover"
                      width={width * 0.06}
                      height={height * 0.03}
                    />
                    <Text style={{fontSize: 14}}> +62</Text>
                  </View>
                </View>

                <TextInput
                  value={phone}
                  placeholder="Nomor Handphone"
                  onChangeText={val => setPhone(val)}
                  mode="outlined"
                  keyboardType="numeric"
                  selectionColor={'#07A9F0'}
                  underlineColor={'#07A9F0'}
                  underlineColorAndroid={'#07A9F0'}
                  theme={{
                    colors: {
                      primary: '#07A9F0',
                      underlineColor: 'transparent',
                    },
                  }}
                  style={{width: '70%', backgroundColor: 'white'}}
                />
              </View>
            )}

            <TextInput
              label="password"
              value={password}
              mode="outlined"
              secureTextEntry={true}
              selectionColor={'#07A9F0'}
              underlineColor={'#07A9F0'}
              underlineColorAndroid={'#07A9F0'}
              onChangeText={val => setPassword(val)}
              theme={{
                colors: {primary: '#07A9F0', underlineColor: 'transparent'},
              }}
              style={{
                width: '90%',
                alignSelf: 'center',
                backgroundColor: 'white',
              }}
            />

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: height * 0.01,
              }}>
              <Text>{!login ? 'Sudah' : 'Belum'} Mendaftar ? </Text>
              <TouchableOpacity onPress={changeLogin}>
                <Text style={{color: '#07A9F0'}}> Klik Disini</Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: height * 0.01,
              }}>
              <TouchableOpacity onPress={forgotTrigger}>
                <Text style={{color: '#07A9F0'}}> Lupa Password ?</Text>
              </TouchableOpacity>
            </View>

            {!login ? (
              <TouchableOpacity
                style={{
                  width: '90%',
                  alignSelf: 'center',
                  marginTop: 16,
                  marginBottom: 16,
                }}
                onPress={SignUp}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 1}}
                  colors={['#0956C6', '#0879D8', '#07A9F0']}
                  style={{padding: 15, borderRadius: 10}}>
                  <Text
                    style={{
                      fontSize: 24,
                      textAlign: 'center',
                      color: 'white',
                    }}>
                    Sign Up
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={{
                  width: '90%',
                  alignSelf: 'center',
                  marginTop: 16,
                }}
                onPress={SignIn}>
                <LinearGradient
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 1}}
                  colors={['#0956C6', '#0879D8', '#07A9F0']}
                  style={{padding: 15, borderRadius: 10}}>
                  <Text
                    style={{
                      fontSize: 24,
                      textAlign: 'center',
                      color: 'white',
                    }}>
                    Sign In
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            )}
          </View>

          {modal && (
            <View
              style={{
                position: 'absolute',
                flex: 1,
                zIndex: 1,
                width: width,
                height: height,
                bottom: 0,
                backgroundColor: 'rgba(255,255,255,0.5)',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={[
                  styles.shadow,
                  {
                    alignSelf: 'center',
                    alignItems: 'center',
                    width: width * 0.8,
                    height: height * 0.2,
                    backgroundColor: 'rgba(255,255,255,1)',
                    marginTop: height * 0.5,
                  },
                ]}>
                <TouchableOpacity
                  style={{alignSelf: 'flex-end', marginRight: width * 0.02}}
                  onPress={forgotTrigger}>
                  <Text style={{fontSize: 20}}>X</Text>
                </TouchableOpacity>
                <TextInput
                  label="Masukkan Email Recoveri"
                  value={email}
                  mode="outlined"
                  keyboardType="email-address"
                  onChangeText={val => setEmail(val)}
                  selectionColor={'#07A9F0'}
                  underlineColor={'#07A9F0'}
                  underlineColorAndroid={'#07A9F0'}
                  theme={{
                    colors: {
                      primary: '#07A9F0',
                      underlineColor: 'transparent',
                    },
                  }}
                  style={{
                    width: '90%',
                    alignSelf: 'center',
                    backgroundColor: 'white',
                    marginBottom: height * 0.01,
                  }}
                />

                <TouchableOpacity onPress={forgotSubmit}>
                  <LinearGradient
                    start={{x: 0, y: 0}}
                    end={{x: 1, y: 1}}
                    colors={['#0956C6', '#0879D8', '#07A9F0']}
                    style={{padding: 10}}>
                    <Text
                      style={{
                        fontSize: 16,
                        textAlign: 'center',
                        color: 'white',
                      }}>
                      Kirim
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
      </KeyboardAwareScrollView> */}

      {loading && <Loading />}

      {/* ----- [START BUTTON BOTTOM OF PAGE] ----- */}
      {/* {!login ? (
        <TouchableOpacity onPress={SignUp}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            colors={['#0956C6', '#0879D8', '#07A9F0']}
            style={{padding: 15}}>
            <Text style={{fontSize: 24, textAlign: 'center', color: 'white'}}>
              Sign Up
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity onPress={SignIn}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            colors={['#0956C6', '#0879D8', '#07A9F0']}
            style={{padding: 15}}>
            <Text style={{fontSize: 24, textAlign: 'center', color: 'white'}}>
              Sign In
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      )} */}
      {/* ----- [END BUTTON BOTTOM OF PAGE] ----- */}
    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 17,
    textAlign: 'center',
  },
  buttonTextColor: {
    color: '#F70161',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    borderRadius: 10,
    width: '100%',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  shadowBlue: {
    shadowColor: '#07A9F0',
    shadowOffset: {
      width: 0,
      height: 11,
    },
    shadowOpacity: 0.55,
    shadowRadius: 14.78,

    elevation: 22,
  },
});
