/* eslint-disable */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {TextInput, HelperText, Avatar} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import LinearGradient from 'react-native-linear-gradient';
import {Picker} from '@react-native-community/picker';

import Appbar from '../../components/appbarHome';
import Loading from '../../components/loading';

// Typography
import {
  material,
  systemWeights,
  materialColors,
  iOSUIKit,
} from 'react-native-typography';

import {URL, formatRupiah} from '../../utils/global';
function updateAccountProfile(props) {
  const placeHolderPhoto =
    'https://images.unsplash.com/photo-1497551060073-4c5ab6435f12?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=667&q=80';

  const [noRek, setNoRek] = useState('');
  const [konfnoRek, setKonfNoRek] = useState('');
  const [namaRekening, setNamaRekening] = useState('');
  const [bank, setBank] = useState('kosong');
  const [branch, setBranch] = useState('');
  const [loading, setLoading] = useState(true);
  const [allBank, setAllBank] = useState([]);
  const [allRek, setAllRek] = useState([]);
  const [id, setId] = useState(0);
  const {height, width} = Dimensions.get('window');
  const urlBank = URL + 'v1/bank';
  const urlRekening = URL + 'v1/account';
  const urlListRekeing = URL + 'v1/account/me';

  useEffect(() => {
    getListBank();
    getRekening();
  }, []);

  const getListBank = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlBank, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setAllBank(responseData.data);
        setLoading(false);
      });
  };

  const getRekening = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);

    let headers = {
      Authorization: `Bearer ${data.token}`,
      'Access-Control-Allow-Origin': '*',
    };

    fetch(urlListRekeing, {headers})
      .then((response) => response.json())
      .then((responseData) => {
        setId(responseData.data[0].id);
        setAllRek(responseData.data);
        setNoRek(responseData.data[0].number);
        setNamaRekening(responseData.data[0].name);
        setBank(responseData.data[0].bank.id);
        setBranch(responseData.data[0].branch);
        setLoading(false);
      });
  };

  const postRekening = async () => {
    console.log('postRekening');
    let dataRincianRekening = [
      {name: 'Bank', value: bank},
      {name: 'Nomor Rekening', value: noRek},
      {name: 'Nama Rekening', value: namaRekening},
      {name: 'Branch', value: branch},
    ];

    let emptyField = dataRincianRekening.find((field) => field.value === '');

    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);
    const id_user = data.id;

    if (!emptyField) {
      if (noRek != konfnoRek) {
        alert('Nomor Rekening Tidak Sama');
      } else {
        setLoading(true);
        var formdata = new FormData();
        formdata.append('bank_id', bank);
        formdata.append('number', noRek);
        formdata.append('name', namaRekening);
        formdata.append('branch', branch);

        let headers = {
          Authorization: `Bearer ${data.token}`,
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'multipart/form-data',
        };

        fetch(urlRekening, {
          method: 'POST',
          headers,
          body: formdata,
        })
          .then((response) => response.json())
          .then(async (responseData) => {
            setLoading(false);
            alert('Rekening Berhasil di Tambahkan');
            console.log(responseData);
          })
          .done();
      }
    } else {
      alert(`Field ${emptyField.name} wajib di isi`);
    }
  };

  const editRek = async () => {
    const value = await AsyncStorage.getItem('data');
    const data = JSON.parse(value);
    console.log(data);
    const id_user = data.id;

    if (noRek != konfnoRek) {
      alert('Nomor Rekening Tidak Sama');
    } else {
      setLoading(true);
      let formdata = new FormData();
      formdata.append('bank_id', bank);
      formdata.append('number', noRek);
      formdata.append('name', namaRekening);
      formdata.append('branch', branch);
      formdata.append('_method', 'put');

      let headers = {
        Authorization: `Bearer ${data.token}`,
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'multipart/form-data',
      };

      fetch(`${urlRekening}/${id}`, {
        method: 'POST',
        headers,
        body: formdata,
      })
        .then((response) => response.json())
        .then(async (responseData) => {
          setLoading(false);
          alert('Rekening Berhasil di Update');
          console.log(responseData);
        })
        .done();
    }
  };

  console.log('allRek', allRek);

  return (
    <View style={{backgroundColor: 'white', flex: 1}}>
      <Appbar params={props} />

      <ScrollView>
        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginVertical: height * 0.01,
          }}>
          <Avatar.Image
            style={{alignSelf: 'center', marginVertical: 18}}
            size={height * 0.2}
            source={{uri: placeHolderPhoto}}
          />
          <Text
            style={[
              iOSUIKit.bodyEmphasized,
              {textAlign: 'center', marginBottom: 18},
            ]}>
            Wahyu Fatur Rizki
          </Text>

          <Text>Nama Lengkap</Text>
          <TextInput
            value={'Wahyu Fatur Rizki'}
            onChangeText={(text) => setNoRek(text)}
            style={{backgroundColor: 'white'}}
            underlineColor={materialColors.blackTertiary}
            underlineColorAndroid={'#F70161'}
            selectionColor={'#F70161'}
            keyboardType={'default'}
          />
          {/* <HelperText style={{color: '#93DCFC', paddingLeft: 0}}>
            Wajib Isi
          </HelperText> */}
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginBottom: height * 0.01,
          }}>
          <Text>Username</Text>
          <TextInput
            value={'wahyufaturrizki'}
            onChangeText={(text) => setKonfNoRek(text)}
            style={{backgroundColor: 'white'}}
            underlineColor={'#07A9F0'}
            underlineColorAndroid={'#07A9F0'}
            selectionColor={'#07A9F0'}
            keyboardType={'numeric'}
          />
          {/* <HelperText style={{color: '#93DCFC', paddingLeft: 0}}>
            Wajib Isi
          </HelperText> */}
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginBottom: height * 0.01,
          }}>
          <Text>Alamat email</Text>
          <TextInput
            value={'wahyu.faturrisky@innovez-one.com'}
            onChangeText={(text) => setNamaRekening(text)}
            style={{backgroundColor: 'white'}}
            underlineColor={'#07A9F0'}
            underlineColorAndroid={'#07A9F0'}
            selectionColor={'#07A9F0'}
          />
          <HelperText style={{color: '#93DCFC', paddingLeft: 0}}>
            Not editable
          </HelperText>
        </View>

        <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginBottom: height * 0.01,
          }}>
          <Text>Nomor Telepon</Text>
          <TextInput
            value={'+62 822 7458 6011'}
            onChangeText={(text) => setBranch(text)}
            style={{backgroundColor: 'white'}}
            underlineColor={'#07A9F0'}
            underlineColorAndroid={'#07A9F0'}
            selectionColor={'#07A9F0'}
          />
          <HelperText style={{color: '#93DCFC', paddingLeft: 0}}>
            Verified contact number
          </HelperText>
        </View>

        {/* <View
          style={{
            width: '90%',
            alignSelf: 'center',
            marginBottom: height * 0.01,
          }}>
          <Text>Nama Bank</Text>
          <Picker
            selectedValue={bank}
            onValueChange={(itemValue, itemIndex) => setBank(itemValue)}
            style={{borderBottomColor: '#07A9F0', borderBottomWidth: 1}}>
            <Picker.Item label={'Pilih Bank'} value={'kosong'} />
            {allBank.map((data, i) => (
              <Picker.Item key={i} label={data.name} value={data.id} />
            ))}
          </Picker>
          <HelperText style={{color: '#93DCFC', paddingLeft: 0}}>
            Wajib Isi
          </HelperText>
        </View> */}

        {/* {loading && <Loading />} */}

        <TouchableOpacity
          // onPress={allRek.length < 1 ? postRekening : editRek}
          onPress={() => alert('Under constructions ...')}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 1}}
            colors={['#0080E0', '#02AAEC', '#67CCF4']}
            style={{
              padding: 15,
              borderRadius: 10,
              width: '90%',
              marginBottom: height * 0.02,
              alignSelf: 'center',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 18, textAlign: 'center', color: 'white'}}>
              {allRek.length < 1 ? 'Save Profile' : 'Ubah'}
            </Text>
          </LinearGradient>
        </TouchableOpacity>

        <View style={{borderTopWidth: 1, borderColor: '#D5D5D5'}} />

        {/* Disabled notif */}
        {/* <View
          style={{
            width: '90%',
            alignSelf: 'center',
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Icon
            name="cloud-upload"
            size={32}
            color="gray"
            style={{marginRight: width * 0.04}}
          />
          <Text style={{width: '90%'}}>
            Masukan rincian Bank Anda dengan teliti. Rincian ini akan digunakan
            untuk pembayaran refund dan komisi.
          </Text>
        </View> */}
      </ScrollView>
    </View>
  );
}

export default updateAccountProfile;
